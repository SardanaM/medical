package com.example.medical.repository;

import com.example.medical.model.Center;
import com.example.medical.model.Doctor;
import com.example.medical.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    Doctor findByEmail(String email);

    List<Doctor> findByType(String type);

    List<Doctor> findAllByCenterAndType(Center center, String type);



}

