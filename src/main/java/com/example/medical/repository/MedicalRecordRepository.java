package com.example.medical.repository;


import com.example.medical.model.MedicalRecord;
import com.example.medical.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, Long> {

    MedicalRecord findByPatient(Patient patient);
}
