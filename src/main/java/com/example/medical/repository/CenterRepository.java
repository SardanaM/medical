package com.example.medical.repository;



import com.example.medical.model.Center;
import com.example.medical.model.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;


public interface CenterRepository extends JpaRepository<Center, Long> {

    Center findByName(String name);
    Center getOne(Long id);
    Center findByDoctors(Doctor d);
    Center getByName(String name);

//    @Lock(value = LockModeType.PESSIMISTIC_READ)
    List<Center> findAll();
}

