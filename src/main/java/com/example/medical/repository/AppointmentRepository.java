package com.example.medical.repository;


import com.example.medical.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    List<Appointment> findAllByCenter(Center center);

    List<Appointment> findAllByPatient(Patient p);
    @Query(value = "select * from appointment where id = any(select appointments_id from doctor_appointments where doctor_id=?1)", nativeQuery = true)
    List<Appointment> findAllByDoctor(Long doctorID);

//    List<Appointment> findAllByDate(Date date);
//
//    Appointment findAppointment(String date, String centre);





}

