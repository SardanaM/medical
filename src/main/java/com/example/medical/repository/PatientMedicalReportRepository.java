package com.example.medical.repository;


import com.example.medical.model.Patient;
import com.example.medical.model.PatientMedicalReport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientMedicalReportRepository extends JpaRepository<PatientMedicalReport, Long> {

    PatientMedicalReport findByPatient(Patient patient);

    PatientMedicalReport findById(long id);
}
