package com.example.medical.service;

import com.example.medical.model.MedicalRecord;
import com.example.medical.model.Patient;
import com.example.medical.repository.MedicalRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;

public interface MedicalRecordService {

    void save(MedicalRecord record);
}
