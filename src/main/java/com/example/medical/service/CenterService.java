package com.example.medical.service;

import com.example.medical.dto.CenterDTO;
import com.example.medical.model.Center;
import org.springframework.http.ResponseEntity;


import java.util.List;

public interface CenterService {

    void save(Center center);

    ResponseEntity<Void> registerCenter(CenterDTO dto);

    ResponseEntity<Center> findCenterById(Long id);

}
