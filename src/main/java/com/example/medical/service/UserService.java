package com.example.medical.service;

import com.example.medical.dto.UserDTO;
import com.example.medical.model.User;
import org.springframework.http.ResponseEntity;


public interface UserService {

    User findByEmail(String email);

    void save(User user);

    ResponseEntity<Void> updateUser(UserDTO dto, String email);
}
