package com.example.medical.service;
import com.example.medical.dto.AppointmentDTO;
import com.example.medical.model.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;
import java.util.List;

public interface AppointmentService {



    void save(Appointment appointment);

//    ResponseEntity<AppointmentDTO> postAppointment(AppointmentDTO dto);

    ResponseEntity<List<AppointmentDTO>> postAppointmentsByPatient(String doctorEmail, String patientEmail);

//    ResponseEntity<AppointmentDTO> postAppointment(String centre, String date);

    ResponseEntity<List<AppointmentDTO>> getAllAppointments();


    ResponseEntity<List<AppointmentDTO>> getAppointmentsCentreWeekly(String centreName);



}
