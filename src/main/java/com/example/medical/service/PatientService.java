package com.example.medical.service;

import com.example.medical.dto.PatientDTO;
import com.example.medical.model.Patient;
import com.example.medical.repository.PatientRepository;
import com.example.medical.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface PatientService {


    Patient findByEmail(String email);

    void save(Patient patient);

    ResponseEntity<Void> addPatient(PatientDTO dto);
}
