package com.example.medical.service;

import com.example.medical.model.Patient;
import com.example.medical.model.PatientMedicalReport;
import com.example.medical.repository.PatientMedicalReportRepository;
import org.springframework.beans.factory.annotation.Autowired;

public interface PatientMedicalReportService {

    void save(PatientMedicalReport patientMedicalReport);
}
