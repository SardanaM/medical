package com.example.medical.service;

import com.example.medical.dto.DoctorDTO;

import com.example.medical.model.Doctor;
import org.springframework.http.ResponseEntity;


public interface DoctorService {


    Doctor findByEmail(String email);
    void save(Doctor doctor);

    ResponseEntity<Void> addDoctor(DoctorDTO dto);
}
