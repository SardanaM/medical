package com.example.medical.impl;

import com.example.medical.dto.CenterDTO;
import com.example.medical.model.Center;
import com.example.medical.model.Doctor;
import com.example.medical.repository.CenterRepository;
import com.example.medical.repository.UserRepository;
import com.example.medical.service.CenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


@Service
public class CenterServiceImpl implements CenterService {


    public CenterRepository centerRepository;

    @Autowired
    public CenterServiceImpl(CenterRepository centerRepository) {
        this.centerRepository = centerRepository;
    }

    public Center findByName(String name) {
        return centerRepository.findByName(name);
    }

    public Center findById(Long id) {
        return centerRepository.findById(id).get();
    }

    public Center findByDoctor(Doctor d) {
        return centerRepository.findByDoctors(d);
    }

    public Center getByName(String name) {
        return centerRepository.getByName(name);
    }


    public void save(Center center) {
        centerRepository.save(center);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED)
    public List<Center> findAllSafe() {
        return centerRepository.findAll();
    }

    public List<Center> findAll() {
        return centerRepository.findAll();
    }

    @Override
    public ResponseEntity<Void> registerCenter(CenterDTO dto) {
        Center c = centerRepository.findByName((dto.getName()));

        if (c == null) {
            Center center = new Center();
            center.setName(dto.getName());
            center.setAddress(dto.getAddress());
            center.setDoctors(new ArrayList<>());
            centerRepository.save(center);
        } else {
            return new ResponseEntity<>(HttpStatus.ALREADY_REPORTED);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Center> findCenterById(Long id) {
        ResponseEntity<Center> resultEntity;

        try {
            Center center = centerRepository.findById(id).orElse(null);
            resultEntity = new ResponseEntity<>(center, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.err.println(e);
            resultEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return resultEntity;
    }
}


