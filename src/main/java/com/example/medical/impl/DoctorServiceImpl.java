package com.example.medical.impl;

import com.example.medical.dto.DoctorDTO;
import com.example.medical.model.Center;
import com.example.medical.model.Doctor;
import com.example.medical.model.User;
import com.example.medical.repository.CenterRepository;
import com.example.medical.repository.DoctorRepository;
import com.example.medical.repository.UserRepository;
import com.example.medical.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DoctorServiceImpl implements DoctorService {


    public DoctorRepository doctorRepository;

    public UserRepository userRepository;

    public CenterRepository centerRepository;


//@Autowired
//    public DoctorServiceImpl(DoctorRepository doctorRepository) {
//        this.doctorRepository = doctorRepository;
//    }

    public Doctor findByEmail(String email) {
        return doctorRepository.findByEmail(email);
    }

    public List<Doctor> findByType(String type) {
        return doctorRepository.findByType(type);
    }

    public List<Doctor> findAllByCentreAndType(Center center, String type) {
        return doctorRepository.findAllByCenterAndType(center, type);
    }

    public void save(Doctor doctor) {
        doctorRepository.save(doctor);

    }

    @Override
    public ResponseEntity<Void> addDoctor(DoctorDTO dto) {
        Doctor d = doctorRepository.findByEmail(dto.getUser().getEmail());
        Center c = centerRepository.findByName(dto.getCentreName());


        if (d != null) {
            return new ResponseEntity<>(HttpStatus.ALREADY_REPORTED);
        }

        if (c == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Doctor doctor = new Doctor(dto);
        doctor.setCenter(c);
        doctorRepository.save(doctor);

        c.getDoctors().add(doctor);
        centerRepository.save(c);


        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}




