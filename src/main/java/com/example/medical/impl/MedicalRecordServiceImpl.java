package com.example.medical.impl;

import com.example.medical.model.MedicalRecord;
import com.example.medical.model.Patient;
import com.example.medical.repository.MedicalRecordRepository;
import com.example.medical.service.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MedicalRecordServiceImpl implements MedicalRecordService {


    private MedicalRecordRepository medicalRecordRepository;

    public MedicalRecord findByPatient(Patient patient) {
        return medicalRecordRepository.findByPatient(patient);
    }

    public void save(MedicalRecord record) {
        medicalRecordRepository.save(record);
    }
}
