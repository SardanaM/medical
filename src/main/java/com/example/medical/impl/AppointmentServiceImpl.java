package com.example.medical.impl;

import com.example.medical.dto.AppointmentDTO;
import com.example.medical.helpers.DateUtil;
import com.example.medical.model.*;
import com.example.medical.repository.*;
import com.example.medical.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Service
@EnableTransactionManagement
public class AppointmentServiceImpl implements AppointmentService {
    public AppointmentRepository appointmentRepository;
    public CenterRepository centerRepository;
    public PatientRepository patientRepository;
    public DoctorRepository doctorRepository;

//    public AppointmentServiceImpl(PatientRepository patientRepository,
//                                  DoctorRepository doctorRepository) {
//        this.patientRepository = patientRepository;
//        this.doctorRepository = doctorRepository;
//
//    }



    public void save(Appointment appointment) {
        appointmentRepository.save(appointment);
    }

    public void delete(Appointment appointment) {
        appointmentRepository.delete(appointment);
    }


    public List<Appointment> findAllByPatient(Patient p) {
        return appointmentRepository.findAllByPatient(p);
    }

    public List<Appointment> findAllByDoctor(Long doctorID) {
        return appointmentRepository.findAllByDoctor(doctorID);
    }

    public List<Appointment> findAllByDoctorAndPatient(Doctor d, Patient p) {
        List<Appointment> apps = appointmentRepository.findAllByCenter(d.getCenter());
        List<Appointment> ret = new ArrayList<Appointment>();
        for (Appointment app : apps) {
            List<Doctor> doctors = app.getDoctors();

            if (app.getPatient() == null) {
                continue;
            }

            if (!app.getPatient().getEmail().equalsIgnoreCase(p.getEmail())) {
                continue;
            }

            for (Doctor doc : doctors) {
                if (doc.getEmail().equalsIgnoreCase(d.getEmail())) {
                    ret.add(app);
                }
            }
        }

        return ret;
    }

    public List<Appointment> findAllByDoctor(Doctor d) {
        List<Appointment> apps = appointmentRepository.findAllByCenter(d.getCenter());
        List<Appointment> ret = new ArrayList<Appointment>();
        for (Appointment app : apps) {
            List<Doctor> doctors = app.getDoctors();

            for (Doctor doc : doctors) {
                if (doc.getEmail().equals(d.getEmail())) {
                    ret.add(app);
                }
            }
        }

        return ret;
    }


//    public List<Appointment> findAllByDate(Date date) {
//        return appointmentRepository.findAllByDate(date);
//    }
//
//    @Override
//    public ResponseEntity<AppointmentDTO> postAppointment(AppointmentDTO dto) {
//        String centre = dto.getCentreName();
//        String date = dto.getDate();
//
//        Appointment appointment = appointmentRepository.findAppointment(date, centre);
//
//        HttpHeaders header = new HttpHeaders();
//        if (appointment == null) {
//            header.set("responseText", "Appointment not found for: (" + date + "," + centre + ")");
//            return new ResponseEntity<>(header, HttpStatus.NOT_FOUND);
//        }
//
//        return new ResponseEntity<>(new AppointmentDTO(appointment), HttpStatus.OK);
//    }

    @Override
    public ResponseEntity<List<AppointmentDTO>> postAppointmentsByPatient(String doctorEmail, String patientEmail) {
        Patient patient = patientRepository.findByEmail(patientEmail);
        Doctor doctor = doctorRepository.findByEmail(doctorEmail);

        if (patient == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (doctor == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<Appointment> app = appointmentRepository.findAllByPatient(patient);
        List<AppointmentDTO> appDTO = new ArrayList<>();

        if (app == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        for (Appointment a : app) {
            appDTO.add(new AppointmentDTO(a));
        }

        return new ResponseEntity<List<AppointmentDTO>>(appDTO, HttpStatus.OK);
    }

//    @Override
//    public ResponseEntity<AppointmentDTO> postAppointment(String centre, String date) {
//        Appointment appointment = appointmentRepository.findAppointment(date, centre);
//
//        HttpHeaders header = new HttpHeaders();
//        if (appointment == null) {
//            header.set("responseText", "Appointment not found for: (" + date + "," + ")");
//            return new ResponseEntity<>(header, HttpStatus.NOT_FOUND);
//        }
//
//        return new ResponseEntity<>(new AppointmentDTO(appointment), HttpStatus.OK);
//    }

    @Override
    public ResponseEntity<List<AppointmentDTO>> getAllAppointments() {
        List<Appointment> app = appointmentRepository.findAll();
        List<AppointmentDTO> appDTO = new ArrayList<AppointmentDTO>();

        if (app == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        for (Appointment a : app) {
            appDTO.add(new AppointmentDTO(a));
        }

        return new ResponseEntity<>(appDTO, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<List<AppointmentDTO>> getAppointmentsCentreWeekly(String centreName) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        Date weekStart = cal.getTime();
        cal.add(Calendar.WEEK_OF_YEAR, 1);
        Date weekEnd = cal.getTime();

        Center center = centerRepository.findByName(centreName);

        if (center == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<Appointment> list = appointmentRepository.findAllByCenter(center);

        if (list == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<AppointmentDTO> dtos = new ArrayList<AppointmentDTO>();

        for (Appointment app : list) {
            if (app.getDate().after(weekStart) && app.getDate().before(weekEnd)) {
                dtos.add(new AppointmentDTO(app));
            }
        }

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


}




