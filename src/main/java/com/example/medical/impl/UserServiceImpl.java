package com.example.medical.impl;



import com.example.medical.dto.UserDTO;
import com.example.medical.model.User;
import com.example.medical.repository.UserRepository;
import com.example.medical.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {
    public UserRepository userRepository;

    public void delete(User user) {
        userRepository.delete(user);
    }


    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User findById(Long id) {
        Optional<User> user = userRepository.findById(id);
        return user.orElse(null);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public ResponseEntity<Void> updateUser(UserDTO dto, String email) {
        User user = userRepository.findByEmail(email);

        if (user != null) {
            user.setUsername(dto.getUsername());
            user.setFirstname(dto.getFirstname());
            user.setLastname(dto.getLastname());
            userRepository.save(user);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
