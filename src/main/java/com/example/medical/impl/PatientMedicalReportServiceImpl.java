package com.example.medical.impl;


import com.example.medical.model.Patient;
import com.example.medical.model.PatientMedicalReport;
import com.example.medical.repository.PatientMedicalReportRepository;
import com.example.medical.service.PatientMedicalReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PatientMedicalReportServiceImpl implements PatientMedicalReportService {

    public PatientMedicalReportRepository patientMedicalReportRepository;

    public void save(PatientMedicalReport patientMedicalReport) {
        patientMedicalReportRepository.save(patientMedicalReport);
    }

    public void delete(PatientMedicalReport patientMedicalReport) {
        patientMedicalReportRepository.delete(patientMedicalReport);
    }

    public PatientMedicalReport findByPatient(Patient patient) {
        return patientMedicalReportRepository.findByPatient(patient);
    }

    public PatientMedicalReport findById(long id) {
        return patientMedicalReportRepository.findById(id);
    }
}
