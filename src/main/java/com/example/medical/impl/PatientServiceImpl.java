package com.example.medical.impl;


import com.example.medical.dto.PatientDTO;
import com.example.medical.model.Patient;
import com.example.medical.model.User;
import com.example.medical.repository.PatientRepository;
import com.example.medical.repository.UserRepository;
import com.example.medical.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class PatientServiceImpl implements PatientService {
    public PatientRepository patientRepository;
    public UserRepository userRepository;

    public Patient findByEmail(String email) {
        return patientRepository.findByEmail(email);
    }

    public void save(Patient patient) {
        patientRepository.save(patient);
    }

    @Override
    public ResponseEntity<Void> addPatient(PatientDTO dto) {
        User user = userRepository.findByEmail(dto.getUser().getEmail());
        String pass = user.getPassword();
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}



