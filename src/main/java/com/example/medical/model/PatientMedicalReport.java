package com.example.medical.model;

import lombok.Data;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import java.util.Date;


@Data
@Entity
public class PatientMedicalReport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description", nullable = true)
    private String description;

    @Column(name = "dateAndTime", nullable = true)
    private Date dateAndTime;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "centre_id")
    private Center center;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;


    public PatientMedicalReport() {
        super();

    }

    public PatientMedicalReport(String description, Date dateAndTime, Doctor doctor, Center center, Patient patient) {
        super();
        this.description = description;
        this.dateAndTime = dateAndTime;
        this.doctor = doctor;
        this.center = center;
        this.patient = patient;


    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Center getCenter() {
        return center;
    }

    public void setCenter(Center center) {
        this.center = center;
    }


    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public static class Builder {
        private String description;
        private Date dateAndTime;
        private Doctor doctor;
        private Center center;
        private Patient patient;


        public Builder(Date dateAndTime) {
            this.dateAndTime = dateAndTime;
        }

        public Builder withDescription(String desc) {
            this.description = desc;

            return this;
        }

        public Builder withDoctor(Doctor d) {
            this.doctor = d;

            return this;
        }

        public Builder withCentre(Center c) {
            this.center = c;

            return this;
        }

        public Builder withPatient(Patient p) {
            this.patient = p;

            return this;
        }


        public PatientMedicalReport build() {
            PatientMedicalReport rp = new PatientMedicalReport();
            rp.setCenter(this.center);
            rp.setDateAndTime(this.dateAndTime);
            rp.setDescription(this.description);
            rp.setDoctor(this.doctor);
            rp.setPatient(this.patient);
            return rp;
        }
    }
}

