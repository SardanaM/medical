package com.example.medical.model;


import lombok.Data;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Data
@Entity
public class Appointment {

    public enum AppointmentType {
        Examination
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "startingDateAndTime")
    private Date date;

    @Column(name = "endingDateAndTime")
    private Date endDate;

    @OneToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    @OneToOne
    @JoinColumn(name = "centre_id")
    private Center center;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Doctor> doctors;

    @Column(name = "appointmentType")
    private AppointmentType appointmentType;

    @Column(name = "predefined")
    private Boolean predefined = false;

    @Column(name = "done")
    private Boolean done = false;

    @Column(name = "confirmed")
    private Boolean confirmed = false;

    private Date newDate;
    private Date newEndDate;


    public Appointment() {
        super();
        this.doctors = new ArrayList<>();
        this.confirmed = true;
        // TODO Auto-generated constructor stub
    }


    public Appointment(Date date, Patient patient, Center center, AppointmentType appointmentType) {
        super();
        this.date = date;
        this.patient = patient;
        this.center = center;
        this.doctors = new ArrayList<>();
        this.appointmentType = appointmentType;
        this.confirmed = true;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Center getCenter() {
        return center;
    }

    public void setCenter(Center center) {
        this.center = center;
    }


    public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    public AppointmentType getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(AppointmentType appointmentType) {
        this.appointmentType = appointmentType;
    }

    public Boolean getPredefined() {
        return predefined;
    }

    public void setPredefined(Boolean predefined) {
        this.predefined = predefined;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Date getNewDate() {
        return newDate;
    }

    public void setNewDate(Date newDate) {
        this.newDate = newDate;
    }

    public Date getNewEndDate() {
        return newEndDate;
    }

    public void setNewEndDate(Date newEndDate) {
        this.newEndDate = newEndDate;
    }


    public static class Builder {
        private Date date;
        private Date endDate;
        private Patient patient;
        private Center center;
        private List<Doctor> doctors;
        private AppointmentType appointmentType;


        public Builder(Date date) {
            this.date = date;
            doctors = new ArrayList<>();
        }


        public Builder withPatient(Patient patient) {
            this.patient = patient;
            return this;
        }

        public Builder withCentre(Center center) {
            this.center = center;
            return this;
        }

        public Builder withEndingDate(Date date) {
            this.endDate = date;
            return this;
        }

        public Builder withDoctors(ArrayList<Doctor> doctors) {
            this.doctors = doctors;
            return this;
        }

        public Builder withType(AppointmentType appointmentType) {
            this.appointmentType = appointmentType;
            return this;
        }


        public Appointment build() {
            Appointment app = new Appointment();
            app.setDate(this.date);
            app.setPatient(this.patient);
            app.setCenter(this.center);
            app.setDoctors(this.doctors);
            app.setAppointmentType(this.appointmentType);
            app.setEndDate(this.endDate);
            return app;
        }

    }



}

