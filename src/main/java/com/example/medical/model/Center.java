package com.example.medical.model;

import lombok.Data;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Center {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @OneToMany()
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Doctor> doctors;


    public Center()
    {
        super();

    }


    public Center(Long id, String name, String address) {
        super();
        this.id = id;
        this.name = name;
        this.address = address;
        this.doctors = new ArrayList<Doctor>();

    }

    public Center(Center center) {
    }

}

