package com.example.medical.model;

import com.example.medical.dto.PatientDTO;

import com.example.medical.helpers.UserBuilder;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Data
@Entity
public class Patient extends User {

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "medicalRecord_id", referencedColumnName = "id")
    private MedicalRecord medicalRecord;


    public Patient()
    {
        super();
    }

    public Patient(String username, String password, String email, String firstname, String lastname) {
        super(username, password, email, firstname, lastname, UserRole.Patient);
        medicalRecord = new MedicalRecord();
        this.setIsFirstLog(false);
    }
    public Patient(PatientDTO dto){
        super(dto.getUser());
        medicalRecord = new MedicalRecord();
    }


    public Patient(User user)
    {
        super(user);
        medicalRecord = new MedicalRecord();
        this.setIsFirstLog(false);
    }

    public MedicalRecord getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(MedicalRecord medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public static class Builder extends UserBuilder
    {

        protected Builder(String email) {
            super(email);

        }

        public Builder withUsername(String username)
        {
            super.withUsername(username);

            return this;
        }


        public Builder withPassword(String password)
        {
            super.withPassword(password);

            return this;
        }


        public Builder withFirstname(String firstname)
        {
            super.withFirstname(firstname);

            return this;
        }

        public Builder withLastname(String lastname)
        {
            super.withLastname(lastname);

            return this;
        }


        public Patient build(){

            User user = super.build();
            Patient p = new Patient(user);
            return p;
        }
    }}





