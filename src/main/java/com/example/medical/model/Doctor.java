package com.example.medical.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;


import com.example.medical.dto.DoctorDTO;
import com.example.medical.helpers.DateUtil;
import com.example.medical.helpers.UserBuilder;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
public class Doctor extends User {
    @Column(name = "type", nullable = true)
    private String type;

    @Column(name = "shiftStart", nullable = true)
    private Date shiftStart;

    @Column(name = "shiftEnd", nullable = true)
    private Date shiftEnd;

    @Column(name = "averageRating", nullable = true)
    private float averageRating;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Center center;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Appointment> appointments;





	/*-----------------------------------------
	***CONSTRUCTORS
	 ------------------------------------------*/


    public Doctor() {


    }

    public Doctor(String username, String password, String email, String firstname, String lastname) {
        super(username, password, email, firstname, lastname, UserRole.Doctor);
        this.setIsFirstLog(true);

    }

    public Doctor(User user) {
        super(user);
        this.setIsFirstLog(true);

    }

    public Doctor(DoctorDTO dto)
    {
        super(dto.getUser());
        this.setIsFirstLog(true);
        this.type = dto.getType();
        this.shiftStart = DateUtil.getInstance().getDate(dto.getShiftStart(), "yyyy-MM-dd HH:mm");
        this.shiftEnd =DateUtil.getInstance().getDate(dto.getShiftEnd(), "yyyy-MM-dd HH:mm");

    }
    /*---------------------------------------
    ***METHODS
     ----------------------------------------*/




    //-------------------------------------------------------------
    public static class Builder extends UserBuilder
    {
        private String type;
        private Date shiftStart;
        private Date shiftEnd;
        public Center center;


        protected Builder(String email) {
            super(email);
        }


        public Builder withUsername(String username)
        {
            super.withUsername(username);

            return this;
        }

        public Builder withPassword(String password)
        {
            super.withPassword(password);

            return this;
        }

        public Builder withFirstname(String firstname)
        {
            super.withFirstname(firstname);

            return this;
        }

        public Builder withLastname(String lastname)
        {
            super.withLastname(lastname);

            return this;
        }




        public Builder withType(String type)
        {
            this.type = type;

            return this;
        }

        public Builder withShiftStart(Date shiftStart)
        {
            this.shiftStart = shiftStart;

            return this;
        }

        public Builder withShiftEnd(Date shiftEnd)
        {
            this.shiftEnd = shiftEnd;

            return this;
        }

        public Builder withCentre(Center center)
        {
            this.center = center;

            return this;
        }


        public Doctor build()        {

            User user = super.build();
            Doctor d = new Doctor(user);
            d.setType(this.type);
            d.setShiftStart(this.shiftStart);
            d.setShiftEnd(this.shiftEnd);
            d.setCenter(this.center);
            return d;
        }
    }
}


