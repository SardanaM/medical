package com.example.medical.dto;


import com.example.medical.model.User;
import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String username;
    private String password;
    private String email;
    private String firstname;
    private String lastname;

    public UserDTO() {
        super();
    }

    public UserDTO(Long id, String username, String password, String email, String firstname, String lastname) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public UserDTO(User dto) {
        super();
        id = dto.getId();
        username = dto.getUsername();
        email = dto.getEmail();
        password = dto.getPassword();
        firstname = dto.getFirstname();
        lastname = dto.getLastname();

    }


}


