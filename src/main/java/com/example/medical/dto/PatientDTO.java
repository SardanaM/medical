package com.example.medical.dto;

import com.example.medical.model.Patient;
import lombok.Data;

@Data
public class PatientDTO {

    private UserDTO user;
    private Long medicalRecordId;

    public PatientDTO() {
        super();
    }

    public PatientDTO(UserDTO user, Long medicalRecordId) {
        super();
        this.user = user;
        this.medicalRecordId = medicalRecordId;;
    }

    public PatientDTO(Patient p)
    {
        UserDTO dto = new UserDTO();
        dto.setUsername(p.getUsername());
        dto.setFirstname(p.getFirstname());
        dto.setLastname(p.getLastname());
        dto.setEmail(p.getEmail());
        this.user = dto;

        if(p.getMedicalRecord() != null)
        {
            this.medicalRecordId = p.getMedicalRecord().getId();
        }
        else
        {
            this.medicalRecordId = Long.valueOf("N/A");
        }
    }



}


