package com.example.medical.dto;

import com.example.medical.helpers.DateUtil;
import com.example.medical.model.Appointment;
import com.example.medical.model.Doctor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data

public class AppointmentDTO {

    private String date;
    private String endDate;
    private String patientEmail;
    private String centreName;
    private String typeOfExamination;
    private String startTimestamp;
    private String newDate;
    private String newEndDate;

    private Boolean done = false;
    private Boolean predefined = false;
    private Boolean confirmed = true;

    private List<String> doctors;

    public AppointmentDTO() {
        super();
        // TODO Auto-generated constructor stub
    }

    public AppointmentDTO(String date, String patientEmail, String centreName, List<String> doctors) {
        super();
        this.date = date;
        this.patientEmail = patientEmail;
        this.centreName = centreName;
        this.doctors = doctors;
        this.done = false;
    }

    public AppointmentDTO(Appointment appointment)
    {
        this.date = DateUtil.getInstance().getString(appointment.getDate(), "yyyy-MM-dd HH:mm");
        if(appointment.getEndDate() != null)
            this.endDate = DateUtil.getInstance().getString(appointment.getEndDate(), "yyyy-MM-dd HH:mm");
        if(appointment.getPatient() != null)
            this.patientEmail = appointment.getPatient().getEmail();
        this.centreName = appointment.getCenter().getName();
        this.doctors = new ArrayList<String>();
        for(Doctor doc : appointment.getDoctors())
        {
            doctors.add(doc.getEmail());
        }

        this.done = appointment.getDone();
        this.predefined = appointment.getPredefined();
        this.confirmed = appointment.getConfirmed();
        if(appointment.getNewDate() != null)
            this.newDate = DateUtil.getInstance().getString(appointment.getNewDate(), "yyyy-MM-dd HH:mm");
        if(appointment.getNewEndDate() != null)
            this.newDate = DateUtil.getInstance().getString(appointment.getNewEndDate(), "yyyy-MM-dd HH:mm");
    }




    }




