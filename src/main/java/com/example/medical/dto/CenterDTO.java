package com.example.medical.dto;

import com.example.medical.model.Center;
import lombok.Data;


@Data
public class CenterDTO {

    private Long id;
    private String name;
    private String address;

    public CenterDTO() {
        super();
    }

    public CenterDTO(Center center) {
        this.id = center.getId();
        this.name = center.getName();
        this.address = center.getAddress();

    }

}

