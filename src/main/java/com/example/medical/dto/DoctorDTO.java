package com.example.medical.dto;


import com.example.medical.helpers.DateUtil;
import com.example.medical.model.Doctor;
import lombok.Data;

@Data
public class DoctorDTO {

    private UserDTO user;
    private String type;
    private String shiftStart;
    private String shiftEnd;
    private String centreName;

    public DoctorDTO() {
        super();
    }

    public DoctorDTO(UserDTO user, String type, String shiftStart, String shiftEnd, String centreName) {
        super();
        this.user = user;
        this.type = type;
        this.shiftStart = shiftStart;
        this.shiftEnd = shiftEnd;
        this.centreName = centreName;
    }

    public DoctorDTO(Doctor d) {
        UserDTO dto = new UserDTO();
        dto.setUsername(d.getUsername());
        dto.setFirstname(d.getFirstname());
        dto.setLastname(d.getLastname());
        dto.setEmail(d.getEmail());
        this.user = dto;
        if (d.getCenter() != null) {
            this.centreName = d.getCenter().getName();
        } else {
            this.centreName = "N/A";
        }

        this.type = d.getType();
        this.shiftStart = DateUtil.getInstance().getString(d.getShiftStart(), "HH:mm");
        this.shiftEnd = DateUtil.getInstance().getString(d.getShiftEnd(), "HH:mm");
    }


}

