package com.example.medical.helpers;


import com.example.medical.model.User;

public class UserBuilder {
    private String username;
    private String email;
    private String password;
    private String firstname;
    private String lastname;


    protected UserBuilder(String email) {
        this.email = email;
    }


    protected UserBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    protected UserBuilder withPassword(String password) {
        this.password = password;

        return this;
    }

    protected UserBuilder withFirstname(String firstname) {
        this.firstname = firstname;

        return this;
    }

    protected UserBuilder withLastname(String lastname) {
        this.lastname = lastname;

        return this;
    }


    protected User build() {
        User user = new User();
        user.setEmail(this.email);
        user.setUsername(this.username);
        user.setPassword(this.password);
        user.setFirstname(this.firstname);
        user.setLastname(this.lastname);
        return user;
    }


}

