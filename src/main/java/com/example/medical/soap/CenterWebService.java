package com.example.medical.soap;

import com.example.medical.model.Center;
import org.springframework.http.ResponseEntity;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface CenterWebService {
    @WebMethod
    ResponseEntity<Center> findCenterById(Long id);
}
