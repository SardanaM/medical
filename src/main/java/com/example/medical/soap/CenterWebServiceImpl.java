package com.example.medical.soap;

import com.example.medical.model.Center;
import com.example.medical.repository.CenterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.NoSuchElementException;

public class CenterWebServiceImpl implements CenterWebService {

    @Autowired
    private CenterRepository centerRepository;

    @Override
    public ResponseEntity<Center> findCenterById(Long id) {
        ResponseEntity<Center> resultEntity;

        try {
            Center center = centerRepository.findById(id).orElse(null);
            resultEntity = new ResponseEntity<>(center, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            System.err.println(e);
            resultEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return resultEntity;
    }
}
