package com.example.medical.soap;

import com.example.medical.dto.CenterDTO;
import com.example.medical.service.CenterService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
public class CenterSoapService {

    @Autowired
    private CenterService centerService;

    @WebMethod
    public void registerCenter(@WebParam(name = "centerDTO") CenterDTO dto) {
        CenterDTO dtoRest = new CenterDTO();
        dtoRest.setName(dto.getName());
        dtoRest.setAddress(dto.getAddress());

        centerService.registerCenter(dtoRest);
    }
}


