package com.example.medical.soap;

import javax.xml.ws.Endpoint;

public class CenterWebServerPublisher {
    {
        String url = "http://localhost:8081/center/id";
        CenterWebService centerWebService = new CenterWebServiceImpl();
        Endpoint.publish(url, centerWebService);
        System.out.println("SOAP service is running at " + url);
    }
}
