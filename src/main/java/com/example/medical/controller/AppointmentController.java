package com.example.medical.controller;


import com.example.medical.dto.AppointmentDTO;
import com.example.medical.helpers.DateUtil;
import com.example.medical.impl.CenterServiceImpl;
import com.example.medical.impl.DoctorServiceImpl;
import com.example.medical.model.*;
import com.example.medical.service.*;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Slf4j
@RestController
@RequestMapping(value = "api/appointments")
@CrossOrigin
@RequiredArgsConstructor
public class AppointmentController {

    private final AppointmentService appointmentService;
    private final CenterServiceImpl centerService;
    private final DoctorServiceImpl doctorService;


//    @PostMapping(value = "/post")
//    // записаться на приемы
//    public ResponseEntity<AppointmentDTO> postAppointment(@RequestBody AppointmentDTO dto) {
//        log.info("Making an appointment at the medical center '{}'.", dto.getCentreName());
//        return appointmentService.postAppointment(dto);
//    }

    @PostMapping(value = "/postAppointments/{doctorEmail}/{patientEmail}")
    //"Назначить приемы"
    public ResponseEntity<List<AppointmentDTO>> postAppointmentsByPatient(@PathVariable("doctorEmail") String doctorEmail, @PathVariable("patientEmail") String patientEmail) {
        log.info("Assigning a patient '{}' to an appointment with a doctor '{}'.", patientEmail, doctorEmail);
        return appointmentService.postAppointmentsByPatient(doctorEmail, patientEmail);
    }

//    @PostMapping(value = "/postAppointment/{centreName}/{date}")
//    //"Записаться на конкретные приемы"
//    public ResponseEntity<AppointmentDTO> postAppointment(@PathVariable("centreName") String centre, @PathVariable("date") String date) {
//        log.info("Making an appointment at the medical center '{}' on '{}'.", centre, date);
//        return appointmentService.postAppointment(centre, date);
//    }

    @GetMapping(value = "/getAll")
    //"Получение всевозможных назначений на приемы"
    public ResponseEntity<List<AppointmentDTO>> getAllAppointments() {
        log.info("Getting all sorts of appointments for appointments.");
        return appointmentService.getAllAppointments();
    }


    @GetMapping(value = "/center/getAllAppointmentsWeek/{centreName}")
    //"Еженедельное получение всех назначений центров"
    public ResponseEntity<List<AppointmentDTO>> getAppointmentsCentreWeekly(@PathVariable("centreName") String centreName) {
        log.info("Weekly receipt of all appointments to the medical center '{}'.", centreName);
        return appointmentService.getAppointmentsCentreWeekly(centreName);
    }


    @GetMapping(value = "/doctor/getAllAppointments/{email}")
    //"Получение всех записей на приемы к докторам"
    public ResponseEntity<List<AppointmentDTO>> getAppointmentsDoctor(@PathVariable("email") String email) {
        log.info("Receiving all records for appointments with doctors by email '{}'.", email);
        Doctor doctor = null;

        try {
            doctor = doctorService.findByEmail(email);
        } catch (ClassCastException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders header = new HttpHeaders();

        if (doctor == null) {
            header.set("responseText", "Doctor not found : (" + email + ")");
            return new ResponseEntity<>(header, HttpStatus.NOT_FOUND);
        }

        List<Appointment> appointments = doctor.getAppointments();
        List<AppointmentDTO> dto = new ArrayList<AppointmentDTO>();

        for (Appointment app : appointments) {
            dto.add(new AppointmentDTO(app));
        }

        if (appointments == null) {
            header.set("responseText", "Appointments not found : (" + email + ")");
            return new ResponseEntity<>(header, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/doctor/getAllAppointmentsByDate/{email}/{date}")
    //"Получение всех записей на приемы к докторам по дате"
    public ResponseEntity<List<AppointmentDTO>> getAppointmentsDoctorByDate(@PathVariable("email") String email, @PathVariable("date") String date) {
        log.info("Retrieve all records for appointments with doctors by email '{}' and date '{}'.", email, date);
        Doctor doctor = null;

        try {
            doctor = doctorService.findByEmail(email);
        } catch (ClassCastException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        HttpHeaders header = new HttpHeaders();

        if (doctor == null) {
            header.set("responseText", "Doctor not found : (" + email + ")");
            return new ResponseEntity<>(header, HttpStatus.NOT_FOUND);
        }

        List<Appointment> appointments = doctor.getAppointments();
        List<AppointmentDTO> dto = new ArrayList<AppointmentDTO>();

        for (Appointment app : appointments) {
            Boolean sameDay = DateUtil.getInstance().isSameDay(app.getDate(), DateUtil.getInstance().getDate(date, "dd-MM-yyyy"));
            if (sameDay) {
                dto.add(new AppointmentDTO(app));
            }
        }

        if (appointments == null) {
            header.set("responseText", "Appointments not found : (" + email + ")");
            return new ResponseEntity<>(header, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


}














