package com.example.medical.controller;
import com.example.medical.dto.DoctorDTO;
import com.example.medical.impl.CenterServiceImpl;
import com.example.medical.impl.DoctorServiceImpl;
import com.example.medical.impl.UserServiceImpl;
import com.example.medical.model.Center;
import com.example.medical.model.Doctor;
import com.example.medical.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;




@Slf4j
@RestController
@RequestMapping(value = "api/doctors")
@CrossOrigin
@RequiredArgsConstructor
public class DoctorController {

    private final DoctorServiceImpl doctorService;



    @PostMapping(value = "/add", consumes = "application/json")
    //"Добавление нового доктора"
    public ResponseEntity<Void> addDoctor(@RequestBody DoctorDTO dto) {
        log.info("Adding a new doctor to the center '{}'.", dto.getCentreName());
        return doctorService.addDoctor(dto);
}}
