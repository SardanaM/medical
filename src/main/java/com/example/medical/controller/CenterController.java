package com.example.medical.controller;

import com.example.medical.dto.CenterDTO;
import com.example.medical.model.Center;

import com.example.medical.impl.CenterServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping(value = "api/center")
@CrossOrigin
@RequiredArgsConstructor
public class CenterController {

    private final CenterServiceImpl centerService;


    @PostMapping(value = "/add", consumes = "application/json")
    //"Создание центра/клиники"
    public ResponseEntity<Void> registerCenter(@RequestBody CenterDTO dto) {
        log.info("Creating a center named '{}'.", dto.getName());
        return centerService.registerCenter(dto);
    }


    @GetMapping(value = "/{id}")
    //"Поиск центра по id"
    public ResponseEntity<Center> findCenterById(@PathVariable("id") Long id) {
        log.info("Search center with id ('{}').", id);
        return centerService.findCenterById(id);

    }
}






