package com.example.medical.controller;

import com.example.medical.dto.PatientDTO;
import com.example.medical.model.User;

import com.example.medical.impl.UserServiceImpl;

import com.example.medical.service.PatientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "api/patient")
@CrossOrigin
@Slf4j
@RequiredArgsConstructor
public class PatientController {

    private final PatientService patientService;
    private final UserServiceImpl userService;

    @PostMapping(value = "/add", consumes = "application/json")
    //"Добавление нового пациента"
    public ResponseEntity<Void> addPatient(@RequestBody PatientDTO dto) {
        return patientService.addPatient(dto);
    }

    private void setUserDeleted(User user) {
        user.setDeleted(true);
        userService.save(user);
    }

}

