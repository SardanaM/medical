package com.example.medical.controller;

import com.example.medical.dto.UserDTO;

import com.example.medical.impl.UserServiceImpl;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping(value = "api/users")
@CrossOrigin
@RequiredArgsConstructor
public class UserController {

    private final UserServiceImpl userService;


    @PutMapping(value = "/update/{email}")
    //"Обновление(добавление, изменение) данных пользователя"
    public ResponseEntity<Void> updateUser(@RequestBody UserDTO dto, @PathVariable("email") String email) {
        log.info("Changing user data with email '{}'.", email);
        return userService.updateUser(dto, email);

    }
}
