import com.example.medical.dto.CenterDTO;
import com.example.medical.service.CenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

import javax.jws.WebMethod;
import javax.jws.WebParam;


@Endpoint
@Component
public class CenterSoapEndpoint {

    private CenterService centerService;

    @Autowired
    public CenterSoapEndpoint(CenterService centerService) {
        this.centerService = centerService;
    }

    @PayloadRoot(namespace = "http://www.baeldung.com/springsoap/gen", localPart = "registerCenterRequest")
    @WebMethod
    public void registerCenter(@WebParam(name = "centerDTO") CenterDTO dto) {
        CenterDTO dtoRest = new CenterDTO();
        dtoRest.setName(dto.getName());
        dtoRest.setAddress(dto.getAddress());

        centerService.registerCenter(dtoRest);
    }
}

